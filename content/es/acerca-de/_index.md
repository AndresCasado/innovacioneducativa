---
title: "Acerca de"
---

## Herramientas

- [Hugo](https://gohugo.io/): generador de webs estáticas.
  - Una web estática es un conjunto de archivos HTML, CSS y JS que se sirven al navegador directamente, sin una aplicación ejecutándose en el servidor ni bases de datos (lo que se llama una web dinámica). Esto requiere muchos menos recursos y es más fácil de mudar de un alojamiento a otro.
  - Se instala el software en el ordenador y, a partir de unos archivos escritos en Markdown, genera los archivos formateados para el navegador.
- [Hextra](https://github.com/imfing/hextra): tema para Hugo.
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/): alojamiento gratuito.
  - GitLab ofrece, además, la posibilidad de ejecutar Hugo cada vez que realizamos un cambio a los archivos fuente en Markdown, posibilitando la actualización del blog usando [git](https://www.freecodecamp.org/espanol/news/guia-para-principiantes-de-git-y-github/), ya sea en un ordenador, en el [móvil](https://termux.dev/en/) o directamente en la web
- [Markdown](https://www.markdownguide.org/getting-started/): lenguaje de marcado.
  - Es un lenguaje sencillo para formatear texto de forma legible utilizado en un sinfín de herramientas (GitHub, GitLab, reddit, foros, generadores estáticos como Hugo, jekyll...)

## ¿Por qué así y no de otra manera?

- Herramientas libres: usando herramientas libres sé que este trabajo estará siempre bajo mi control y que podré alojarlo donde mejor me convenga. También puede ser utilizado por otras personas para crear sus propias páginas o que incluso [mejoren *esta* página]().
- [Separación de contenido y presentación](https://en.wikipedia.org/wiki/Separation_of_content_and_presentation)
  - El contenido está escrito en Markdown y organizado mediante su estructura de archivos y carpetas. La presentación depende del tema elegido. Si decido dejar de usar *Hextra* y utilizar otro tema, no tengo que reescribir ni adaptar el contenido. De hecho originalmente utilicé [*PaperCSS*](https://github.com/zwbetz-gh/papercss-hugo-theme).
  - Por la misma razón suelo utilizar y recomendar [LaTeX](https://www.latex-project.org/) para escribir documentos.

Puedes ver el código fuente de la web en [GitLab](https://gitlab.com/AndresCasado/innovacioneducativa).

Puedes seguir el blog mediante [RSS](https://andrescasado.gitlab.io/innovacioneducativa/index.xml).
- [Wikipedia sobre RSS](https://es.wikipedia.org/wiki/RSS)
- Lector libre y gratuito para Android: [Feeder](https://github.com/spacecowboy/Feeder)
