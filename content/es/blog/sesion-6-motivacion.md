---
title: "Sesión 6 - Matemáticas invisibles y recursos digitales"
date: 2023-10-23 17:00:00
---

En esta sesión vemos cómo motivar el aprendizaje de las matemáticas,
y un conjunto de recursos digitales para crear materiales didácticos.

<!--more-->
---

A veces recuerdo cuando, después de enseñar a una chica a integrar en
unas clases particulares, le pregunté que si sabía para qué servía todo
eso y me dijo que no tenía ni idea.

La motivación no es un problema exclusivo del alumnado "que no aprende",
sino que supone un problema también para quien sí consigue aprender. ¿Qué
sentido tiene saber derivar e integrar sin saber en qué situación o
problema van a poder aplicarlo? ¿Qué interés van a tener en retener la
información en su cerebro una vez acaben el curso o la secundaria?

Los recursos recogidos en la presentación sirven para llamar su atención
o ver las _matemáticas invisibles_, y son sin duda una buena manera de motivar
a empezar el aprendizaje. Sin embargo, la mayoría son recursos pasivos, en
las que no se pone en marcha del todo el cerebro. Por eso creo que es importante
también trabajar la motivación desde un punto de vista más activo, con objetivos
a largo plazo. Podemos intentarlo con tareas o propuestas en las que
el alumnado pueda trabajar de forma activa y sienta que su esfuerzo
produce resultados aplicables al mundo real.

De todos los recursos, creo que el que podría encajar en este enfoque sería el
de las olimpiadas matemáticas, pero, viendo los problemas que se proponen ([enlace](http://www.olimpiadamatematica.es/platea.pntic.mec.es/_csanchez/olimprab.htm),
[enlace archivado](https://web.archive.org/web/20230610204645/http://www.olimpiadamatematica.es/platea.pntic.mec.es/_csanchez/loc2020.html)),
parece que se centran demasiado en problemas abstractos y algebraicos. Quizás sería mejor un
enfoque más similar al **aprendizaje basado en proyectos**, del cual podemos encontrar
algunos [recursos en el INTEF](https://cedec.intef.es/recursos-abp-para-matematicas/).