---
title: "Sesión 4 - Neuroeducación"
date: 2023-10-09 17:00:00
---

Esta sesión parte de una base neurológica del aprendizaje, explicando al final cómo poner
estos conocimientos en práctica en el aula.

<!--more-->
---

De todo lo que trata el tema, creo que lo que más me ha impactado es el efecto Pigmalión.
Resulta sorprendente que la percepción de un o una docente pueda influir tanto en los
resultados académicos del alumnado.  

Otro aspecto que me ha parecido importante de la neuroeducación es la importancia de las
emociones. Parece que las convenciones sociales del mundo adulto y laboral nos han
convencido de que debemos ser más emocionales y estoicos, de que debemos mantener la
seriedad. Y aunque no es que sea una persona muy efusiva, nunca me ha gustado esa manera
de ver las cosas, por lo que me alegra ver que, científicamente, haría bien en intentar
que mis clases no prioricen la seriedad sino la emoción.

Igual que no recuerdo muchos de los contenidos que estudié en el instituto, temo que 
los detalles de esta sesión los olvidaré. Por eso creo que lo que debo llevarme son dos
cosas.

Primero, deberé esforzarme para disfrutar mis clases. Si yo no disfruto enseñando, difícilmente
mi alumnado podrá disfrutar aprendiendo. Y si eso no ocurre, ya hemos visto que el
aprendizaje no será efectivo.

Segundo, aunque sean chicos y chicas adolescentes y su conducta a veces se vea afectada por
su maduración actual, no debo utilizarlo como excusa para justificar un bajo rendimiento,
sino como aliado para dirigir ese fascinante cambio neurológico a un aprendizaje emocional y
con significado.
