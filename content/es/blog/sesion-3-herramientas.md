---
title: "Sesión 3 - Herramientas digitales"
date: 2023-10-02 17:00:00
---

Durante la primera parte de esta sesión vimos una serie de herramientas dedicadas principalmente a la curación de contenidos.

<!--more-->
---

Como ya indiqué en uno de los artículos sobre la primera sesión,
soy un defensor y promotor del software libre, no por sus aportaciones técnicas,
sino por sus aportaciones sociales.

Por eso, he pensado en crear una lista de software que me parece interesante para su uso en la docencia,
ya sea como herramienta de trabajo para los y las docentes como para su uso en el aula.

Se puede encontrar en la sección superior, y la iré modificando para añadir
herramientas según vaya encontrando algunas que puedan ser útiles, así como
las que utilizo ya en mi día a día trabajando y estudiando.
