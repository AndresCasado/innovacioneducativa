---
title: "Sesión 10 - Presentación de proyectos (II)"
date: 2023-11-27 17:00:00
---

Segunda sesión de presentación de Proyectos de Innovación Educativa.

<!--more-->
---

## Tercer proyecto: Tesoro geométrico del antiguo Egipto

Ambientada en la cultura que construyó las pirámides, esta _escape room_
nos enfrentó a diferentes grupos en una carrera por resolver una serie de
retos. Estos retos incluían operaciones matemáticas, conocimientos teóricos
y tareas manipulativas.

Si bien la variedad de los ejercicios es un aspecto muy positivo y la
ambientación hace que sea más amena, hubo algunos incidentes en la resolución
de los mismos por falta de rigor matemático. Por ejemplo, en el crucigrama
podemos encontrar definiciones imprecisas como
"Poliedro el cual todas sus caras son triángulos", cuya respuesta pretendida
era "pirámide". Sin embargo, sin irnos más lejos, las pirámides de Egipto son 
de base cuadrada. Otras definiciones se prestan a contraejemplos, como
"Cuerpo redondo limitado por una superficie curva y dos caras planas circulares".
La respuesta pretendida es "cilindro", pero si, por ejemplo, seccionáramos
una esfera con dos planos paralelos, la porción que quedaría en el centro entraría
dentro de esa definición.

Sería raro que un alumno o alumna de 3º de la ESO encontrara estas alternativas,
pero nuestra labor como docentes de matemáticas es enseñarles, entre otras cosas,
a pensar de esta manera, _out of the box_ como dicen en inglés.

Es un proyecto bien trabajado, combina colaboración dentro del grupo con competición
contra otros grupos, motivando a jugar y haciendo que todo el mundo participe, por lo que
lo único que habría de mejorar sería la falta de rigor.

## Cuarto proyecto: Trivial matemático
Una vuelta de tuerca del clásico Trivial, cambiando las diferentes temáticas
por diferentes bloques del contenido de matemáticas y añadiendo algunas casillas
más "entretenidas" estilo "Puedes volver a tirar los dados" o "No puedes seguir
jugando este turno".

La idea es sencilla, pero la ejecución se notó demasiado improvisada: las reglas
no estaban del todo claras al principio y se decidieron sobre la marcha (¿cada equipo tira
una vez? ¿o si acertamos seguimos jugando?), y al ser tantas personas participando en tan
pocos grupos era fácil que se quedara gente sin jugar.

El gran tamaño del tablero y los dados resulta llamativo, pero creo que termina
haciendo más torpe el juego.

Introducir preguntas de historia de las matemáticas es interesante, pero eran
muy difíciles incluso para nosotros, por lo que no creo que sean del todo
adecuadas para 3º de la ESO, y tampoco creo que sirva para aprenderla.

En general, aunque puede que cumpla como entretenimiento, creo que no terminaría
de ser del todo didáctica en un aula de 3º de la ESO.

## Quinto proyecto: Cuadrado fraccionado
Nuestro proyecto, similar por casualidad a "Matemáticos por un día", se centra
en las demostraciones de propiedades a través de la manipulación de piezas
geométricas, aunque en nuestro caso se trataba de las fracciones y sus operaciones.

Durante la realización de la actividad, pudimos detectar un fallo, y es que hay
operaciones como 1/4 + 2/5 y 2/3 que dan resultados muy parecidos. Eso no lo
habíamos contemplado, y puede dar lugar a confusiones, aunque por
otro lado también se puede aprovechar para explicar la densidad de los números
racionales.

La última operación, la división, supuso un reto comprenderla, tal y como esperábamos,
y salvo alguna persona parece que se terminó entendiendo bastante bien, por lo que
podemos decir que no resultará trivial para el alumnado de 3º de la ESO.

Personalmente no confiaba del todo en que la ejecución y la recepción en el aula
fuera a ser del todo buena, pero aparentemente se entendió la intención y gustó
bastante, hasta el punto de que varias personas nos pidieron copias de las piezas,
así que creo que podemos considerar la actividad un éxito.
