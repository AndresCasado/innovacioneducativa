---
title: "Sesión 2 - Formación"
date: 2023-09-25
---

Uno de los temas de esta sesión es la formación para docentes, tanto
previa como una vez ejerciendo.

<!--more-->

## Recursos digitales de formación para docentes

La Administración dispone de varias colecciones de recursos digitales para el personal docente. Podemos encontrar desde [guías sobre ciberseguridad y privacidad online](https://aseguratic.intef.es/) hasta [situaciones de aprendizaje](https://intef.es/recursos-educativos/situaciones-aprendizaje/) (antes llamadas _unidades didácticas_).

### INTEF
Los dos ejemplos anteriores pertenecen al [INTEF](https://intef.es/), cuyo responsable es el Ministerio de Educación y Formación Profesional. Sería imposible detallar todo el contenido disponible, pero podemos hacernos una idea con el [mapa del sitio](https://intef.es/mapa-de-sitio/).

Quizás uno de los recursos más destacables para la especialidad de Matemáticas sea [MatesGG](https://intef.es/recursos-educativos/matesgg/), una colección de recursos creados con [GeoGebra](https://www.geogebra.org/about). Esta herramienta de código abierto permite visualizar fácilmente muchos de los contenidos de la asignatura, así como compartir los recursos y editarlos. Para señalar su popularidad, basta ver los perfiles recomendados en otra sesión posterior, de los cuales tres mencionan GeoGebra en su biografía:
- [Pablo Triviño](https://nitter.net/p_trivino)
- [Alejandro Gallardo](https://nitter.net/alegallardo28)
- [Débora Pereiro](https://nitter.net/debora_pereiro)

Volviendo a la página del INTEF, otra de las secciones destacables es la de [competencias digitales](https://intef.es/competencia-digital-educativa/competencia-digital-del-alumnado/). Si bien no es muy extensa, será importante tenerla de referencia en el futuro. Además de los marcos legales, incluye algunas situaciones de aprendizaje de ejemplo que podemos utilizar con el alumnado.

También existen recopilaciones de herramientas digitales orientadas a la educación que podemos aprovechar en nuestras clases. Por ejemplo, esta sobre [entornos virtuales de aprendizaje](https://intef.es/formacion/te-ayudamos/entornos-virtuales-de-aprendizaje/), cuya utilidad se hizo patente durante el confinamiento del COVID-19, pero que también podría ser útil en entornos educativos diferentes, como la educación en centros de adultos, médicos o penitenciarios.

### CTIF

El [Centro Territorial de Innovación y Formación (CTIF)](https://innovacionyformacion.educa.madrid.org/) es el cuerpo administrativo de la comunidad de Madrid que se encarga de la innovación educativa. El tipo de contenido es similar al del INTEF, aunque hay menor cantidad en general, y parece que predominan los eventos.

A diferencia de la página del INTEF, la [sección del CTIF sobre competencia digital](https://innovacionyformacion.educa.madrid.org/competencia-digital) es mucho más completa, ya que además de la legislación incluye inscripciones a cursos de formación y métodos de certificación de competencias. Parece, por tanto, que la competencia sobre las competencias digitales, valga la redundancia, recae sobre las comunidades autónomas.

### Otras comunidades autónomas

Por curiosidad, he buscado de qué portales disponen en otras comunidades autónomas.

- [Junta de Extremadura](https://formacion.educarex.es)

- [Junta de Andalucía](https://www.juntadeandalucia.es/educacion/portals/web/ced)

- [Generalitat de Catalunya](https://xtec.gencat.cat/ca/innovacio/)

### Educalab

En el temario se indicaba que, anteriormente, el INTEF se llamaba [Educalab](http://educalab.es/intef/formacion),
y aunque su web de momento permanece (se pueden encontrar recursos como [Blender: 3D en la educación](http://educalab.es/-/blender-3d-en-la-educacion)),
no parece estar muy claro que se vaya a conservar para siempre.
El curso enlazado no parece estar disponible en la web actual del INTEF, así que es de suponer que
su conservación depende de la de la web de Educalab.

## Problemas

Quitando el problema del mantenimiento en línea de recursos antiguos, estas webs sufren los problemas
que normalmente sufren las webs de las administraciones públicas. Por un lado, el diseño a veces juega
en contra del usuario. Aunque el diseño visual no sea siempre el más atractivo o actualizado, es la
organización la que más dificulta la experiencia de usuario: a veces las categorías no son muy claras,
hay subsecciones difíciles de encontrar... Así, se siente que hay que poner más de nuestra parte en 
encontrar recursos que lo necesario al navegar en otras plataformas.

Otra desventaja, relacionada con la organización, es que es difícil encontrar dónde están los últimos
recursos que se han añadido a la web. A veces la sección "Novedades" incluye noticias, pero no hay una
noticia por cada recurso publicado; otras veces, sólo listan eventos presenciales u online a los que
apuntarse, que si bien son de interés, no son todo lo que podemos encontrar en estas páginas.

Para solucionar este problema, las organizaciones suelen enlazar a Twitter (ahora llamada X). El problema con Twitter (ahora llamada X) es
que ya [no permite navegar libremente por la red sin iniciar sesión](https://www.infobae.com/tecno/2023/07/06/twitter-se-retracta-y-deja-ver-tuits-sin-iniciar-sesion-una-decision-tras-la-llegada-de-threads/),
sólo acceder a tweets concretos (ahora llamados posts). Aún es posible utilizar proxies alternativos 
como [Nitter](nitter.net) ([lista de instancias](https://github.com/zedeus/nitter/wiki/Instances)),
pero igual que pasó con los proxies alternativos de Reddit, LibReddit y Teddit, pueden dejar de
funcionar en cualquier momento.

Idealmente, las administraciones deberían optar por utilizar [RSS](https://es.wikipedia.org/wiki/RSS),
de manera que sea fácil consultar y filtrar las novedades, así como automatizar la notificación
mediante bots en redes sociales, en aplicaciones de mensajería o directamente lectores de RSS.
El estándar RSS era tan utilizado en el internet de hace una década, ¡_que incluso podías seguir
cuentas de Twitter o de Facebook mediante RSS_!

Lo curioso es que, en el temario de la asignatura, se incluía una captura de la web del INTEF donde se
podía ver el icono del estándar RSS:

![Captura de la web de INTEF, señalando el icono de rSS](INTEFRSS_marcado.png "Captura incluida en el temario de la asignatura" )

Algunas páginas aún tienen RSS disponible, como la de [Redinred](https://redined.educacion.gob.es/xmlui/), la de la Junta de Extremadura o la [Junta de Andalucía](https://www.juntadeandalucia.es/educacion/portals/web/ced/rss). Si no, mientras funcione, Nitter también ofrece la posibilidad de
convertir una cuenta de Twitter en un _feed_ RSS.

No es realmente difícil crear un _feed_ RSS ([¡este blog tiene uno!](../../index.xml)), sólo requiere 
la voluntad de hacerlo bien, ya que suele estar implementado en los gestores de contenido 
(Wordpress, Joomla, etc...).
