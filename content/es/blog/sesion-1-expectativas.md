---
title: "Sesión 1 - Expectativas"
date: 2023-09-18
---
En la sesión de introducción a la asignatura, además de los detalles burocráticos como las ponderaciones de las actividades de evaluación o la guía docente, hemos empezado reflexionando sobre la **innovación educativa**, a priori sin conocimientos sobre la misma.

<!--more-->
---

## Expectativas
A juzgar por los ejemplos, parece que los puntos fuertes son la **ludificación** (o  más comúnmente gamificación) y las **nuevas tecnologías**, no sólo para que las apliquemos con el futuro alumnado sino para nosotros y nosotras mismas.

Puede que la gamificación sea uno de varios métodos que vayamos a estudiar este curso. Las matemáticas normalmente se consideran una asignatura difícil, y es importante tener un buen repertorio de métodos diferentes para explicarlas.

Por otro lado, con la revolución informática disponemos de gran cantidad de herramientas que facilitan tareas como por ejemplo la representación de funciones y figuras geométricas o el cálculo de derivadas y límites, entre muchos otros.
¿Cómo podemos motivar al alumnado a aprender matemáticas cuando parece que las hace todas el ordenador?

Imagino que la asignatura consistirá en estudiar la mejor manera de implantar la gamificación y las nuevas tecnologías en el aula, partiendo de por qué funcionan para atrapar la atención y generar motivación en el alumnado y evitando alejarnos de la materia que realmente tenemos que enseñar, en el caso de nuestra especialidad, las matemáticas.

Cabe destacar, sin embargo, que pese al nombre oficial de la asignatura, la profesora no habla de _TICs_ sino de **recursos educativos**. Esto ya parece dar pistas de que, pese a la importancia que las nuevas tecnologías puedan tener, no lo son _todo_, existen más opciones a la hora de innovar que, en lugar de simplemente utilizar aparatos nuevos, implementan **ideas** nuevas.
