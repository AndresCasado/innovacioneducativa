---
title: "Sesión 5 - Redes sociales"
date: 2023-10-16 17:00:00
---

En esta ocasión, Oriol Borrás-Gené nos presentó diferentes tecnologías 
y maneras de utilizarlas en la docencia.

<!--more-->
---

A grandes rasgos, esta sesión se separa en dos grandes partes. Por un lado,
las redes sociales y aplicaciones para transmitir contenidos, y por otro,
la inteligencia artificial y, concretamente, ChatGPT.

# Sobre las redes sociales
Las redes sociales, y concretamente Twitter, fueron muy importantes para mí, ya
que gracias a ellas he descubierto géneros musicales, juegos y, sobre todo,
personas que ahora forman parte de mi vida.

Sin embargo, incluso antes de que cierto millonario con ideas más que cuestionables
comprara la plataforma, su toxicidad era más que palpable. No sólo la de Twitter:
Facebook y el escándalo de Cambridge Analytica o el mecanismo de recompensa 
intermitente de TikTok son algunas muestras de que las grandes plataformas no son
gratuitas, sino que pagamos con nuestra privacidad, nuestra salud mental e incluso
con la estabilidad de los estados democráticos.

Usar plataformas con los llamados "algoritmos de recomendación" se ha convertido en
algo no muy lejos de rebuscar en un vertedero. Abrir tweets requiere ahora que hayas
iniciado sesión (o usar proxies como [Nitter](https://nitter.net)), y las respuestas
a cualquier tweet están llenas de cuentas con "tick azul" respondiendo sin aportar
simplemente para autopromocionarse o meter ruido.

Además, el odio de la ultraderecha es cada vez más y más palpable (cualquiera diría que [el 
dueño de Twitter asiste a meetings de ultraderecha](https://www.lavanguardia.com/opinion/20231219/9459315/estrella-delshow-meloni.html))
y sólo va a más.

Por eso abandoné Twitter, o lo que quedaba, y pese a que echo de menos a la gente
que sólo conocía por ahí, ahora mi vida es más tranquila: no me enfado con desconocidos,
no me paso 4 horas haciendo scroll ni estoy constantemente informado de las penurias del
mundo.

Sobre este tema considero importante este vídeo, **The Internet is Worse Than Ever – Now What?**
([enlace privado](https://redirect.invidious.io/watch?v=fuFlMtZmvY0) / [enlace directo a YouTube](https://www.youtube.com/watch?v=fuFlMtZmvY0))

Como alternativa, existe el llamado "fediverso",
un conjunto de servidores y plataformas compatibles entre sí (igual que existen distintos
servidores de email). Sin embargo, ni siquiera los servicios autogestionados están [libres de amenazas](https://es.wired.com/articulos/que-es-la-revolucion-del-fediverso-y-como-threads-la-amenaza).

Sería posible, de hecho, crear un nodo del fediverso dedicado exclusivamente a la educación,
y aunque no entra en mis planes a corto plazo, no descarto hacerlo en un futuro.


# Sobre la inteligencia artificial

Sueno a profe anticuado incluso antes de ser profe y sin ser nada anticuado (en cuanto a tecnología),
pero me cuesta ver con optimismo la irrupción de los grandes modelos de lenguaje en la
educación. 

Desde ahora es imposible mandar para casa proyectos que sustituyan los exámenes. Si ya era difícil
utilizar buscadores y descartar las decenas de blogs con información redundante o inútil escrita por personas,
ahora además tenemos máquinas capaces de generar decenas de blogs con información _errónea_ cada segundo.

Ahora hay gente llamándose a sí misma "artista" por pedirle a una matriz de números de 10GB que dibuje
algo. ¿Se ha considerado alguna vez alguien chef por pedirle a un cocinero que le prepare un plato?
¿Consideraremos al alumnado capaz de aprender porque sepan pedirle a una máquina que les dé algo 
estadísticamente diseñado para parecer una respuesta correcta?

En el fondo quiero pensar que estoy [tan equivocado como lo hemos estado siempre](https://www.lavozdegalicia.es/xlsemanal/a-fondo/estamos-en-una-crisis-de-valores-sociedad-decadencia-moral.html), y confío en que la pasión innata
del ser humano perseverará, pero lo hará a pesar de la IA, y no gracias a ella.


# Sobre mi pesimismo

Mi pesimismo es algo personal y, por supuesto, no juzgo a nadie individualmente por usar unas u
otras herramientas. Este pesimismo no nace de un miedo a lo desconocido ni de una superioridad moral,
sino de la decepción por las promesas incumplidas.

Estar más conectados iba a ayudarnos a conocer otras culturas, apreciar más lo diferente y hacer del
mundo un lugar más pequeño, y lo que ha hecho ha sido engancharnos a base de generar odio y nos ha
aislado socialmente.

La automatización y, en última instancia, la inteligencia artificial, iba a reducir la carga de 
trabajo de la clase trabajadora, nos iba a permitir vivir más y dedicarnos con más libertad a 
esas pasiones humanas que son la música, la poesía o la pintura. Lo que tenemos son robots
haciendo dibujos y escribiendo mientras las personas seguimos obligadas a ser más y más productivas
trabajando las mismas horas.

Esto no quiere decir que estas tecnologías no tengan ni vayan a tener lados positivos, pero
a mí, personalmente, no me salen a cuenta.
