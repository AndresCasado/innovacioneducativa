---
title: "Conclusiones"
date: 2023-12-18 17:00:00
---

Cerramos el portfolio con unas conclusiones sobre la asignatura.

<!--more-->
---

La asignatura es sin duda la que más contenidos tiene de todo el cuatrimestre,
la que más áreas trata de cubrir y, quizás, la que más cueste poner en práctica
en el futuro.

Neurología, multimedia y nuevas tecnologías, metodologías... hay tantas cosas
que probar y sobre las que aprender que puede llegar a ser abrumador. Sin embargo,
la sensación con la que cierro la asignatura son ganas de observar qué pasa en las
aulas durante las prácticas, de hablar con los chicos y las chicas sobre qué piensan
y cómo se sienten en las clases de matemáticas, y, en general, de aprender a enseñar
mejor.

Me gustaría empezar, cuando tenga tiempo, a tratar de formar esa red alternativa de recursos
y plataformas libres que tanto digo que hace falta, y
en las aulas probar las _flipped classroom_, que me han parecido la opción más interesante
de cambiar el funcionamiento habitual de las clases.
