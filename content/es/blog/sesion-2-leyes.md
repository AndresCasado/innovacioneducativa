---
title: "Sesión 2 - Leyes educativas"
date: 2023-09-25
---

Esta sesión comienza hablando del **sistema educativo** y definiendo las partes relevantes al ejercicio de la docencia. ¿Cómo fomenta, o más bien, frena la innovación el sistema educativo? La legislación detalla cómo deben funcionar los centros docentes (ratios, evaluaciones), qué contenidos se enseñan, con qué objetivos, y las obligaciones de los docentes, como la formación que deben tener y mantener.

<!--more-->
---

## Cambios constantes
En un [artículo enlazado en el temario](https://www.20minutos.es/noticia/4480192/0/leyes-educacion-espana-50-anos/) podemos ver un breve repaso a la historia de la legislación en nuestro país, que destaca por el constante cambio al que la han sometido los dos grandes partidos tras la dictadura.

El polarizado debate que provoca parece tener consenso en que España es el único país en el que se modifica tanto la ley de Educación, aunque personalmente me cuesta creer que seamos el único país del mundo que no se ha puesto de acuerdo. ¿Acaso no hay polarización en ningún otro país? ¿Están de acuerdo los _tories_ y los _labours_ en Reino Unido? ¿Los socialistas, los _verdes_ y los conservadores en Alemania? 

Un vistazo rápido a la Wikipedia sobre el [sistema educativo francés](https://fr.wikipedia.org/wiki/Syst%C3%A8me_%C3%A9ducatif_en_France#L'%C3%A9ducation_nationale), en la sección sobre la educación nacional, enumera muchas fechas en las que se introdujeron cambios. Personalmente, no conozco la historia de estos cambios, pero mi pregunta es, ¿la conocen quienes aseguran que nosotros, en España, somos la excepción? ¿No será que gusta más lo ajeno por ajeno que por bueno?

## Más allá de las leyes
Siempre se pone de ejemplo a Finlandia como el país con el mejor sistema educativo. Sin entrar a discutir cuál es la métrica que define que un sistema sea mejor o peor, [esta entrevista](https://www.eldiario.es/catalunya/archivo-de-educacion-de-catalunyaplural-cat/xavier-melgarejo-finlandesa-servicio-comunidad_132_5562799.html) al pedagogo catalán Xavier Melgarejo da su visión sobre el sistema finés y sus diferencias con el sistema educativo español.

A relación de los constantes cambios, podemos destacar esta cita:
> En Finlandia la descentralización es tan grande que el 90% del currículo se elabora municipalmente, pero para hacer eso tienes que formar a los maestros para que puedan crear contenidos y confiar en ellos.

Quizás la ley estatal finlandesa no haya cambiado en mucho tiempo, ¿pero significa eso que no haya cambiado la educación en sí? ¿Qué acogida podría tener un modelo así en España? Ya hay fuertes críticas al sistema autonómico por parte de algunos sectores políticos y sociales de nuestro país, pese al gran peso que mantiene el Estado sobre la ley educativa. ¿Qué dirían si se delegara no en las comunidades autónomas, sino en municipios?

¿Podría ser un [problema económico](https://www.eldiario.es/sociedad/ascensor-social-educacion-roto-vidas-marcadas-codigo-postal-cuna_1_10347009.html)? Si la brecha económica entre clases influye tanto en el futuro académico y laboral del alumnado, cabe pensar que la ley educativa es sólo un factor de muchos, y quizás ni siquiera el mayor. ¿Podría nuestro país y nuestra economía aspirar a tener el escudo social que tiene Finlandia?

¿Es si no un [problema cultural](https://www.eldiario.es/euskadi/euskadi/educacion-crear-seres-empleables-integros_128_3626234.html)? En esa entrevista a César Bona, considerado uno de los mejores profesores de España, señala las diferencias en algunos valores culturales, como la importancia que se le da aquí a las notas, al prestigio social de la comunidad docente o a la sobreconsideración que recibe el informe PISA.

## Conclusión

Por un lado, creo que es importante conocer y reflexionar sobre nuestras leyes y compararlas con las de otros territorios, con el fin de buscar la mejora de las mismas. Pero también me da la sensación de que a veces pecamos de ser demasiado negativos, pesimistas, como si aquí nunca se hiciera nada bien. Quizás sería positivo aprender a valorar más los avances, sin que ello conlleve a perder el espíritu crítico, que es quien ha logrado esos avances, en primer lugar.
