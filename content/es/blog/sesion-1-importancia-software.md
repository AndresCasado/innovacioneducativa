---
title: "Sesión 1 - La importancia del software"
date: 2023-09-18
---

<!--more-->
---

Como persona preocupada por la libertad digital, trato siempre de usar 
**tecnologías libres**, y me preocupa la escasez de herramientas libres en las 
listas que se han mostrado.  La mayoría son servicios web que de alguna manera 
requerirán crearse una cuenta, que tratarán de recabar datos, que se quedarán 
con el contenido que generemos y no podremos moverlo de una plataforma a otra. 
Sin ir más lejos, en un Kahoot que hemos hecho nos hemos tenido que juntar 
porque ahora Kahoot sólo ofrece de manera gratuita 10 usuarios simultáneos.

¿Debemos permitir que la educación de los estudiantes dependa de las decisiones 
de plataformas *privadas*? Me alivia ver que hay docentes que se preocupan por 
estas cuestiones, como por ejemplo Manu Velasco, recomendado por la profesora 
en una de las diapositivas. Este profesor mantiene un blog para compartir 
recursos con la comunidad educativa, y al abrirlo me he llevado la grata 
sorpresa de ver una [publicación reciente](https://www.ayudaparamaestros.com/2018/09/24-herramientas-de-software-libre-para.html) que enlazaba, a su vez, a 
una publicación del [CEDEC](https://cedec.intef.es/) (Centro Nacional de Desarrollo Curricular en Sistemas no Propietarios).

En esa publicación, el CEDEC da a conocer diferentes herramientas de
software libre.
No conocía esta entidad, aunque sí me suena ver el
logo de la aplicación _eXeLearning_.

Creo que esta asignatura, si bien va a ser muy interesante, puede terminar
siendo una muestra de una dependencia excesiva en la tecnología
y que genera una brecha digital de cuya existencia no siempre somos conscientes.