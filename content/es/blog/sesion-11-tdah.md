---
title: "Sesión 11 - Dificultades del aprendizaje"
date: 2023-12-11 17:00:00
---

En la última clase vemos dos dificultades de aprendizaje: la _discalculia_ y el
mal llamado _trastorno de déficit de atención con/sin hiperactividad_.

<!--more-->
---

# Discalculia
Conocí la discalculia leyendo a una chica que sigo en internet que comentó casualmente
que la tenía, pero me costaba imaginar y ponerme en el lugar de una persona discalcúlica.
Al fin y al cabo, si quiero dedicarme a las matemáticas y las ciencias de la computación
es porque se me dan muy bien las matemáticas. La tabla de multiplicar colores fue iluminador
en ese sentido, pero aparte de las preguntas típicas sobre cómo se detecta y cómo se trata,
me surge la duda sobre la posición como docente de matemáticas y la relación en el proceso
de enseñanza-aprendizaje.

¿Ayuda tener conocimientos más amplios, profundos y abstractos de las matemáticas, como
por ejemplo tenemos quienes venimos de matemáticas puras? ¿O pueden suponer ese conocimiento
y pasión un obstáculo para guiar correctamente a estas personas? Es decir, ¿pueden entenderse
quienes tienen una facilidad casi innata con quienes tienen una dificultad?

Por lo que parece, la literatura científica sobre la discalculia aún es relativamente
joven, así que será la experiencia, si se da el caso, la que responda a esas preguntas.
