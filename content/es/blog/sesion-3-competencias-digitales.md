---
title: "Sesión 3 - Competencias digitales"
date: 2023-10-02 17:10:00
---

En la segunda parte de esta sesión vimos qué eran las competencias digitales y
qué objetivo tratan de conseguir.

<!--more-->
---

En un mundo completamente informatizado, para bien y/o para mal, es necesario
que el entorno educativo sea capaz de desenvolverse con las herramientas necesarias,
tanto para usarlas a su favor en la labor de la docencia como para poder transmitir
al alumnado conocimientos sobre ella. La evaluación de las **Competencias Digitales**
pretende asegurar que el equipo docente y del centro dispondrá de las capacidades
necesarias para ello.

## Puntos de información sobre Competencia Digital

El [INTEF](https://intef.es/competencia-digital-educativa/) es sin duda el punto
del que surge toda la documentación que sirve de referencia a las comunidades
autónomas. Por otro lado, dentro de las webs regionales, la web de la 
[Comunidad de Madrid](https://innovacionyformacion.educa.madrid.org/competencia-digital)
parece más elaborada que otras como, por ejemplo, las de la
[Junta de Andalucía](https://www.juntadeandalucia.es/educacion/portals/web/transformacion-digital-educativa/competencia-digital)
o la de
[Castilla La Mancha](https://educamosclm.castillalamancha.es/portal/formacion-digital)
.

En mi opinión, la web del INTEF es demasiado densa para ser leída desde un punto de
vista práctico como docentes, ya que la información que ofrece consta mayormente de
documentación y de enlaces a más documentación. De esto mismo sufre la web de la
Junta de Andalucía. El otro extremo es la web de Castilla La Mancha, que simplemente
ofrece vídeos, con las desventajas que ello conlleva (no poder buscar en el texto,
no poder ajustar el ritmo de lectura, entre otras). Por tanto, la web de la Comunidad 
de Madrid parece el mejor sitio para consultar información ahora mismo.

## Opinión personal sobre Competencia Digital

Mi mayor preocupación en cuanto a las competencias digitales radica en el proceso
de adquisición y certificación. Dividir las competencias en distintos niveles y áreas
da libertad a la hora de especializarse, pero siento que puede saturar y abrumar.

Por otro lado, leyendo las evidencias fundamentales del nivel B1.1 de la
[Guía de
evaluación de la competencia digital docente](https://dgbilinguismoycalidad.educa.madrid.org/docs/nube/D39_Guia_evaluacion_CDD_22_23_1681720526.pdf)
se puede ver en la rúbrica de evaluación lo siguiente:
>- Dispone de un LMS/CMS actualizado
>- Tiene matriculados a sus estudiantes
>- El LMS/CMS está estructurado por temas o secciones
>- ...

No creo que sea responsabilidad de un/a docente implantar en un centro educativo
un LMS o CMS, sino del propio centro. Por tanto, la evaluación final del equipo
docente dependerá del centro.

Entiendo así que la responsabilidad de la formación en competencias digitales
deberá ser dirigida por el centro, y que desde el punto de vista de una persona que
sólo se dedique a la docencia, el foco deberá estar en lo que el centro le asigne
o le comunique.
