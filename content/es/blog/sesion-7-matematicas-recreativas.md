---
title: "Sesión 7 - Matemáticas recreativas"
date: 2023-11-06 17:00:00
---

En esta sesión hablamos de herramientas para divulgar las matemáticas con
un enfoque ameno y divertido.

<!--more-->
---

# Matemáticas recreativas

Las matemáticas no sólo son herramientas presentes en las partes "serias" del día a día,
sino que podemos encontrarlas en el arte o en los juegos o incluso hacer arte o juegos
con ellas. A lo largo de esta sesión hemos visto una gran colección de recursos divulgativos
de las matemáticas con este enfoque.

Tal y como parece decir implícitamente la presentación, es fácil ver una justificación
en la neurología para aplicar esta visión a la enseñanza: si conseguimos sorprender a la clase
y despertar su interés conseguiremos que aprendan más fácilmente.

Entiendo que esta clase de contenido ha de usarse con mesura: un uso abusivo puede quitarle
el factor novedoso y hacer que pierda ese potencial motivador, o peor incluso, que dejen de
tomarse la clase en serio.

Como todo lo visto hasta ahora, si bien parecen conceptos sencillos, su aplicación en el aula
debe ser rigurosa, tener objetivos marcados y evaluarse correctamente.

# Herramientas libres

De nuevo vuelvo a las andadas y propongo alternativas raras para cuando llegue el mundo
post-big-tech.

En esta ocasión, dado que la sesión incluye los vídeos como herramienta educativa,
la que traigo aquí es [PeerTube](https://joinpeertube.org/es).

PeerTube es una plataforma federada de alojamiento de vídeos. Como su nombre indica, se
asemeja a YouTube: puedes subir vídeos a un canal, comentarlos, seguir un canal... La diferencia
está en que los servidores en los que se aloja no son una gran
[nube secarríos](https://tunubesecamirio.com/somos/), sino que pueden ser pequeños servidores
localizados incluso en el propio instituto.

Gracias a que es federada, no necesitamos cuenta en todos los servidores. Igual que podemos
mandar emails desde @urjc.es a @ucm.es, podemos seguir canales o comentar vídeos de PeerTube
desde una única cuenta en una plataforma federada con ActivityPub, ¡incluso Mastodon!

Desgraciadamente, esta opción aún es minoritaria, y atrae principalmente a la _cultura hacker_,
por lo que el contenido que vamos a encontrar girará mayormente en torno a Linux, programación
o metaconversación sobre cultura libre. La ventaja, por otro lado, es que mucho material estará
disponible bajo licencia [Creative Commons](https://creativecommons.org/).

En cualquier caso, he podido encontrar
[algunos vídeos](https://peertube.uno/w/ko5o872448ydHggVAHX97Y)
sobre matemáticas. Con suerte en el futuro habrá más.
