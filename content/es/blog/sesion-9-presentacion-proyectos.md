---
title: "Sesión 9 - Presentación de proyectos"
date: 2023-11-20 17:00:00
---

Primera sesión de presentación de Proyectos de Innovación Educativa.

<!--more-->
---

## Primer proyecto: Matemáticos por un día

Este proyecto comienza explicando que, a lo largo de la actividad, nos
pondremos en el papel de auténticos matemáticos y conseguiremos demostrar
algunas propiedades geométricas y algebraicas.

Para ello se propusieron diferentes tareas que debían resolverse con unas
piezas de goma eva recortadas con formas de triángulos, cuadrados y
rectángulos. Así, terminamos demostrando las identidades notables y el
teorema de Pitágoras de manera algebraica a partir de una construcción
geométrica.

Se nota la influencia de las dos personas con estudios puramente matemáticos
del grupo, pues esa pasión por las demostraciones rigurosas y llamativas
es lo que hace que la actividad sea motivadora. De hecho, en mi caso
nunca había visto esa demostración del teorema de Pitágoras y ahora
sin duda no la voy a olvidar.

En general me parece una actividad muy interesante y bien ejecutada.

## Segundo proyecto: Math It!

Math It! es un juego de cartas basado en el juego Dobble, que consiste en
un conjunto de cartas con diferentes dibujos, de tal manera que cualquier
par de cartas tiene uno y sólo uno en común.

El juego presentado le da una vuelta a esta idea y sustituye los dibujos 
por operaciones matemáticas, de tal manera que los jugadores y las
jugadoras deben computarlas para saber qué número coincide y poder así
coger la carta o dejar la suya, dependiendo del modo de juego.

La idea es estupenda y la prueba en clase es sin duda un éxito. Quienes
probamos el juego nos enganchamos en cuestión de segundos. Además, no
sólo se practican matemáticas, sino que pudimos aprender algo nuevo
durante la explicación de cómo hicieron el juego ([explicación](https://thewessens.net/ClassroomApps/Main/finitegeometry.html?topic=geometry&id=19)).

Como pegas, diría que los números utilizados tenían unos "ojitos"
que a veces dificultaban su lectura, y que parece que el foco está
demasiado en la "agilidad de cálculo", que es una habilidad que
personalmente no considero demasiado importante.