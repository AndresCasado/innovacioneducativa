---
title: "Sesión 8 - Más vídeos y metodologías activas"
date: 2023-11-13 17:00:00
---

Acabamos de ver la utilidad de los vídeos y lo enlazamos con la
_flipped classroom_ y la _gamificación_.

<!--more-->
---

# Más herramientas libres para vídeos

Puesto que seguimos tratando los vídeos, sigo buscando y proponiendo alternativas libres.

Existe un canal de YouTube, en inglés, que destaca entre otras cosas por sus
animaciones matemáticas:
[**3blue1brown**](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw).

Buscando software para crear animaciones matemáticas encontré uno que se llama
[**Manim**](https://www.manim.community/)
y podemos instalarlo libremente en nuestro ordenador.

Resulta que es un _fork_ (derivado) del software que usa _3blue1brown_ mantenido por la
comunidad. La pega es que hay que saber programar en Python... pero Python es un lenguaje
diseñado para ser sencillo, por lo que con un poco de ganas, todo se puede.

¡Otro software libre más que nos ayuda en la labor docente, y otro canal para utilizar en
clase!

# Metodologías activas

En esta parte, hemos visto principalmente dos herramientas educativas: la _flipped classroom_
o clase invertida y la _gamificación_ o _ludificación_ (para quienes quieren usar un prefijo
y sufijo latinos).

## _Gamificación_

Como persona aficionada a los videojuegos, pensar en _gamificar_ la enseñanza **me aterra**.
La complejidad que supone el diseño de videojuegos y juegos en general no es algo para tomarse
a la ligera, especialmente cuando dentro del juego hay que implementar sistemas de economía.
¿Cuántas monedas asigno como valor a una tarea? ¿Y a una recompensa canjeable? ¿Y a una
penalización?

La implementación de un sistema de economía en un juego individual (_The Witcher 3_) da para
[una hora de conferencia](https://www.youtube.com/watch?v=SdYSRkXqhyk).

Claro está que no hace falta llegar a esos niveles de complejidad, pero podemos hacernos
una idea de lo tremendamente delicado que es, y un sistema mal diseñado (e incluso uno bien
diseñado) será
[inevitablemente explotado](https://youtu.be/GMtIAXtAGxw)
por los jugadores y jugadoras (o, en nuestro caso, alumnos y alumnas).

Sólo con este problema ya me cuesta plantearme desarrollar este método de enseñanza, y aún falta
por comprender la dificultad de crear una narrativa que enganche, que considere los gustos de
toda la clase, de crear ejercicios que evalúen correctamente las competencias buscadas...

No cierro puertas a utilizar experiencias _gamificadas_ ya diseñadas cuya eficacia esté
demostrada, pero creo que es una herramienta al alcance de personas muy concretas.

## _Flipped classroom_

Invertir las clases me parece más asequible y, en relación esfuerzo-resultados, creo
que es mucho más rentable implementarlo. Sobre el papel funciona muy bien, y tras la más que
probable fricción inicial, seguramente la clase terminará trabajando mejor.

Sin embargo, me pregunto hasta qué punto es posible escalar su implementación al contenido de
todo el curso y, todavía más allá, de todas las asignaturas. ¿Hasta qué punto puede el alumnado
gestionar su autoaprendizaje en casa con 7 u 8 materias? ¿Cuánta coordinación requiere por parte
del profesorado?

También es cierto que estos problemas son algo que ya ocurre con los deberes, por lo que no hay
tanto que perder al intentar implementarlo.









