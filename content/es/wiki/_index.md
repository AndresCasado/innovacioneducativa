---
title: Wiki
---

## Herramientas
### Redes sociales
#### Mastodon
Link: [Mastodon](https://joinmastodon.org/es)

La más popular de las plataformas del "fediverso", Mastodon es una plataforma
de microblogging (estilo Twitter) que podremos instalar en cualquier servidor o,
como opción más habitual, utilizar a través de algún servidor ya creado.

Existen páginas para encontrar servidores como [instances.social](https://instances.social/) o la propia
página de [joinmastodon.org](https://joinmastodon.org/es/servers)

Podemos encontrar algunos servidores con temáticas muy concretas, como por
ejemplo [Andalucia.social](https://andalucia.social/about).

#### PeerTube
Link: [PeerTube](https://joinpeertube.org/es)

Parte de la red del "fediverso", esta plataforma es estilo YouTube, alojamiento de
vídeo incluido. Dado que el alojamiento de vídeos es más costoso, los servidores
suelen requerir alguna clase de pago o donativo.

Si sólo queremos seguir cuentas o comentar vídeos, podemos usar cualquier otra plataforma
del fediverso, como Mastodon, PixelFed, Lemmy...

También cuenta con un [buscador de contenidos y servidores](https://joinpeertube.org/instances).

### Manim
Link: [Manim](https://www.manim.community/)

Este software de animación matemática permite transmitir conceptos
complejos de manera muy visual. Requiere conocimientos de Python.

### Nextcloud 

Link: [Nextcloud](https://nextcloud.com/es)

![Captura de pantalla de Nextcloud](Nextcloud/files.png)

Nextcloud es una plataforma que nos permite crear nuestra propia nube. La 
instalación más básica nos permite almacenar y compartir archivos directamente,
pero también editar documentos en línea.

Además, se pueden instalar _apps_ para expandir sus capacidades a muchas otras
necesidades. La lista de apps disponibles se encuentra [aquí](https://apps.nextcloud.com/).

La Comunidad de Madrid tiene su propio servidor de [ownCloud](https://cloud.educa.madrid.org), que es el software [del cual surgió Nextcloud](https://medium.com/@IntelliSoft/the-great-cloud-storage-debate-owncloud-vs-nextcloud-which-one-is-right-for-you-fb901d79def3).

**Pros**:
- Privada: nuestros datos no salen de nuestros servidores si no queremos
- Trabajo en la "nube": existen clientes para todas las plataformas, así que
ya sea en nuestro ordenador personal, en nuestro móvil, o en el navegador web
de un ordenador prestado, podremos acceder a nuestros datos.

**Contras**:
- Necesitamos alguien que sea capaz de configurar el servidor y mantenerlo.
- Para que esté disponible a todo internet, y no sólo a una red local, deberemos tener muy en cuenta la ciberseguridad.
- Debemos financiar el servidor de algún modo, aunque a veces un ordenador antiguo es un buen sitio en el que alojarlo.

#### Nextcloud Bookmarks

Link: [Nextcloud Bookmarks](https://apps.nextcloud.com/apps/bookmarks)

![Captura de pantalla de Nextcloud Bookmarks](Nextcloud/bookmarks.png)

Este complemento para Nextcloud nos permite organizar enlaces mediante carpetas
y etiquetas, buscar, compartir, crear RSS de las colecciones...

Puede ser una buena alternativa a Symbaloo, Wakelet y otras herramientas de
curación de contenidos.
