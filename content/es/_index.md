---
---

![Banner](img/banner.png)

¡Hola! Soy **Andrés Casado** y esta es la página en la que compartiré todo lo relacionado con la asignatura _Innovación Educativa y TICs Aplicadas a la Enseñanza de las Matemáticas_ del Máster de Profesorado, curso 2023-2024, impartida por Raquel Garrido Abia.



{{< cards >}}
    {{< card link="blog" title="Blog" icon="pencil-alt" >}}
    {{< card link="wiki" title="Wiki" icon="book-open" >}}
    {{< card link="acerca-de" title="Acerca de" icon="question-mark-circle" >}}
{{< /cards >}}

